import React from "react";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import { SaveIcon } from "../theme/icons.js";
import { MinimizeIcon } from "../theme/icons.js";
import Tooltip from "@material-ui/core/Tooltip";
import { UndoIcon } from "../theme/icons.js";
import { RedoIcon } from "../theme/icons.js";
const styles = theme => ({
  root: {},
  disabled: {
    opacity: ".3"
  },

  iconsWrapper: {
    // background: "red !important",
    position: "absolute",
    top: theme.spacing.unit,
    right: theme.spacing.unit,
    "& svg": { fill: theme.palette.primary.contrastText }
  }
});

class ModuleControls extends React.Component {
  state = {
    name: []
  };

  handleChange = event => {
    this.setState({ name: event.target.value });
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <div className={classes.iconsWrapper}>
        <Tooltip title="Undo">
          <IconButton aria-label="Search" className={classes.disabled}>
            <UndoIcon />
          </IconButton>
        </Tooltip>

        <Tooltip title="Redo">
          <IconButton aria-label="Search" className={classes.disabled}>
            <RedoIcon />
          </IconButton>
        </Tooltip>

        <Tooltip title="Save">
          <IconButton aria-label="Search">
            <SaveIcon />
          </IconButton>
        </Tooltip>

        <Tooltip title="Minimize">
          <IconButton aria-label="Search">
            <MinimizeIcon />
          </IconButton>
        </Tooltip>

      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(ModuleControls);
