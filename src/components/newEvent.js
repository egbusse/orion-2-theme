import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { colorOrange } from "../theme/colors.js";

const styles = theme => ({
  hexbutton: {
    background: colorOrange,
    clipPath: "polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%)",
    fontSize: "2em",
    letterSpacing: "0",
    width: "2em",
    height: "calc((2em * 100) / 86.603) !important",
    lineHeight: "calc((2em * 100) / 86.603) !important"
  },
  glowbutton: {
    filter: "drop-shadow(0px 10px 20px " + colorOrange + "88)"
  }
});
class ResponsiveDialog extends React.Component {
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { fullScreen } = this.props;
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.glowbutton}>
          <Button className={classes.hexbutton} onClick={this.handleClickOpen}>
            +
          </Button>
        </div>
        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">
            {"CREATE EVENT"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>Lorem ipsum dolor sit amet.</DialogContentText>
            <TextField
              autoFocus
              id="name"
              label="Email Address"
              type="email"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              CANCEL
            </Button>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              CREATE
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ResponsiveDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(ResponsiveDialog);
