import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  root: {
    backgroundImage:
      "url(https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/3cfU-gridBG.svg),linear-gradient(-135deg, #3d3d3d 0%, #000000 100%)",
    backgroundSize: "cover"
  },
  button: {
    margin: theme.spacing.unit
  },

  disabled: {},

  input: {
    display: "none"
  },
  title: {
    display: "inline-block !important",
    margin: "1em "
  }
});

function RaisedButtons(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        {" "}
        <Typography className={classes.title} variant="title" gutterBottom>
          RAISED BUTTONS
        </Typography>
        <CardContent>
          <Button variant="raised" className={classes.button}>
            Default
          </Button>
          <Button variant="raised" className={classes.button} color="primary">
            Primary
          </Button>
          <Button variant="raised" className={classes.button} color="secondary">
            Secondary
          </Button>
          <Button
            variant="raised"
            color="secondary"
            disabled
            className={classes.button}
          >
            Disabled
          </Button>
          <Button
            variant="raised"
            href="#raised-buttons"
            className={classes.button}
          >
            Link
          </Button>
          <input
            accept="image/*"
            className={classes.input}
            id="raised-button-file"
            multiple
            type="file"
          />
          <label htmlFor="raised-button-file">
            <Button
              variant="raised"
              component="span"
              className={classes.button}
            >
              Upload
            </Button>
          </label>
        </CardContent>
      </Card>
    </div>
  );
}

RaisedButtons.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(RaisedButtons);
