import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    backgroundImage:
      "url(https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/3cfU-gridBG.svg),linear-gradient(-135deg, #3d3d3d 0%, #000000 100%)",
    backgroundSize: "cover"
  },
  button: {
    margin: theme.spacing.unit,
    boxShadow: "none"
  },
  disabled: {},

  input: {
    display: "none"
  },
  title: {
    display: "inline-block !important",
    margin: "1em "
  }
});

function OutlineButtons(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <Typography className={classes.title} variant="title" gutterBottom>
          OUTLINE BUTTONS
        </Typography>
        <CardContent>
          <div>
            <Button variant="outlined" className={classes.button}>
              Default
            </Button>
            <Button
              variant="outlined"
              color="primary"
              className={classes.button}
            >
              Primary
            </Button>
            <Button
              variant="outlined"
              color="secondary"
              className={classes.button}
            >
              Secondary
            </Button>
            <Button variant="outlined" disabled className={classes.button}>
              Disabled
            </Button>
            <Button
              variant="outlined"
              href="#outlined-buttons"
              className={classes.button}
            >
              Link
            </Button>
            <input
              accept="image/*"
              className={classes.input}
              id="outlined-button-file"
              multiple
              type="file"
            />
            <label htmlFor="outlined-button-file">
              <Button
                variant="outlined"
                component="span"
                className={classes.button}
              >
                Upload
              </Button>
            </label>
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
OutlineButtons.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OutlineButtons);
