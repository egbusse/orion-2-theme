import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

const styles = theme => ({
  root: {
    backgroundImage:
      "url(https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/3cfU-gridBG.svg),linear-gradient(-135deg, #3d3d3d 0%, #000000 100%)",
    backgroundSize: "cover"
  },
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing.unit
  },

  title: {
    display: "inline-block !important",
    margin: "1em "
  }
});
const inputstyle = {
  //border: "solid 1px red"
};

class ComposedTextField extends React.Component {
  state = {
    name: "Composed TextField"
  };

  handleChange = event => {
    this.setState({ name: event.target.value });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <Typography className={classes.title} variant="title" gutterBottom>
            TEXTFIELDS
          </Typography>{" "}
          <CardContent>
            <div className={classes.container}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="name-simple">Name</InputLabel>
                <Input
                  id="name-simple"
                  value={this.state.name}
                  onChange={this.handleChange}
                  style={inputstyle}
                />
              </FormControl>

              <FormControl
                className={classes.formControl}
                aria-describedby="name-helper-text"
              >
                <InputLabel htmlFor="name-helper">Name</InputLabel>
                <Input
                  id="name-helper"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
                <FormHelperText id="name-helper-text">
                  Some important helper text
                </FormHelperText>
              </FormControl>
              <FormControl className={classes.formControl} disabled>
                <InputLabel htmlFor="name-disabled">Name</InputLabel>
                <Input
                  id="name-disabled"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
                <FormHelperText>Disabled</FormHelperText>
              </FormControl>
              <FormControl
                className={classes.formControl}
                error
                aria-describedby="name-error-text"
              >
                <InputLabel htmlFor="name-error">Name</InputLabel>
                <Input
                  id="name-error"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
                <FormHelperText id="name-error-text">Error</FormHelperText>
              </FormControl>
            </div>{" "}
          </CardContent>{" "}
        </Card>
      </div>
    );
  }
}

ComposedTextField.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ComposedTextField);
