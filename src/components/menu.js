import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Mock from "./mock.js";
import { ProfileIcon } from "../theme/icons.js";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { navBG } from "../theme/backgrounds.js";
import { hexBG } from "../theme/backgrounds.js";
import InboxIcon from "@material-ui/icons/Inbox";
import DraftsIcon from "@material-ui/icons/Drafts";
import { colorOrange } from "../theme/colors.js";
import CardMedia from "@material-ui/core/CardMedia";

//import { mailFolderListItems, otherMailFolderListItems } from './tileData';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundImage:
      "url(https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/3cfU-gridBG.svg),linear-gradient(-135deg, #3d3d3d 0%, #000000 100%)",
    backgroundSize: "cover",
    zIndex: 1,
    overflow: "hidden",

    display: "flex"
  },
  menulist: {
    background: "none"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,

    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,

    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  hide: {
    display: "none"
  },
  drawerPaper: {
    borderImage: "url('data:image/svg+xml;utf8," + navBG + "')",
    borderImageSlice: "40%",
    borderImageRepeat: "repeat",
    borderImageWidth: "10px 10px 10px 0px",
    border: "10px solid transparent",
    position: "initial",
    overflowX: "hidden",
    height: "calc(100% - 20px)",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9
    }
  },

  wrapper: {
    position: "relative",
    height: "1050px",
    overflowX: "visible",
    "&>div": {
      height: "100%"
    }
  },
  toolbar: {
    background: "url('data:image/svg+xml;utf8," + hexBG + "')",
    backgroundSize: "60%",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "50% 50%",

    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    position: "absolute",
    right: "0",
    top: "50%",
    transform: "translateX(50%) translateY(-50%)",
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3
  },
  logo: {
    borderImage: "url('data:image/svg+xml;utf8," + navBG + "')",
    borderImageSlice: "40%",
    borderImageRepeat: "repeat",
    borderImageWidth: "10px 10px 10px 0px",

    width: "100%",

    "& div:nth-of-type(1)": {
      height: "72px",
      width: "72px",

      display: "inline-block",
      backgroundSize: "60%",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "50% 50%"
    },

    "& div:nth-of-type(2)": {
      height: "72px",
      width: "6.5em",
      display: "inline-block",
      backgroundSize: "contain",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "50% 50%"
    }
  }
});

class MiniDrawer extends React.Component {
  state = {
    open: false
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes, theme } = this.props;
    const { anchorEl } = this.state;
    return (
      <div className={classes.root}>
        {" "}
        <div className={classes.wrapper}>
          <Drawer
            variant="permanent"
            classes={{
              paper: classNames(
                classes.drawerPaper,
                !this.state.open && classes.drawerPaperClose
              )
            }}
            open={this.state.open}
          >
            {" "}
            <div className={classes.toolbar}>
              <IconButton
                onClick={
                  this.state.open === true
                    ? this.handleDrawerClose
                    : this.handleDrawerOpen
                }
                className={this.state.open === true ? "" : "offset"}
              >
                {this.state.open === true ? (
                  <ChevronLeftIcon />
                ) : (
                  <ChevronRightIcon />
                )}
              </IconButton>
            </div>
            <div className={classes.logo}>
              <CardMedia
                className={classes.media}
                image="https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/OMF6-logo1.svg"
                title="logo"
              />
              <CardMedia
                className={classes.media}
                image="https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/JfiU-logo2.svg"
                title="logo"
              />
            </div>{" "}
            <Divider />
            <List className={classes.menulist}>
              <ListItem button>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="EVENTS" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="SOMETHING ELSE" />
              </ListItem>
            </List>
            <Divider />
            <List className={classes.menulist}>
              {" "}
              <ListItem button>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="ANOTHER ITEM" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="NETWORK MAP" />
              </ListItem>
            </List>
          </Drawer>{" "}
        </div>
        <main className={classes.content}>
          <Mock />
        </main>
      </div>
    );
  }
}

MiniDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(MiniDrawer);
