import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    backgroundImage:
      "url(https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/3cfU-gridBG.svg),linear-gradient(-135deg, #3d3d3d 0%, #000000 100%)",
    backgroundSize: "cover"
  },
  button: {
    margin: theme.spacing.unit
  },
  disabled: {},

  input: {
    display: "none"
  },
  title: {
    display: "inline-block !important",
    margin: "1em "
  }
});

function FlatButtons(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        {" "}
        <Typography className={classes.title} variant="title">
          FLAT BUTTONS
        </Typography>
        <CardContent>
          <Button className={classes.button}>Default</Button>
          <Button color="primary" className={classes.button}>
            Primary
          </Button>
          <Button color="secondary" className={classes.button}>
            Secondary
          </Button>
          <Button disabled className={classes.button}>
            Disabled
          </Button>
          <Button href="#flat-buttons" className={classes.button}>
            Link
          </Button>
          <input
            accept="image/*"
            className={classes.input}
            id="flat-button-file"
            multiple
            type="file"
          />
          <label htmlFor="flat-button-file">
            <Button component="span" className={classes.button}>
              Upload
            </Button>
          </label>
        </CardContent>
      </Card>
    </div>
  );
}
FlatButtons.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FlatButtons);
