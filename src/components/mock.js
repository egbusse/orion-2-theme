import React from "react";
import ReactDOM from "react-dom";
import { colorLightest } from "../theme/colors.js";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Textfields from "./textfields.js";
import NewEvent from "./newEvent.js";

import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import CardMedia from "@material-ui/core/CardMedia";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  disabled: {},

  input: {
    display: "none"
  },
  main: {
    position: "relative",
    //padding: "1em",
    height: "100%",

    "&>div": {
      paddingTop: "1em",
      //background:"red",
      display: "flex"
    },

    "&>div:nth-of-type(2)": {
      position: "absolute !important",

      right: ".5em",
      bottom: ".5em"
    }
  },

  title: {
    display: "inline-block"
  },
  card: {
    maxWidth: 645,
    position: "relative",
    overflow: "visible",
    marginRight: "1em",

    "&:before": {
      position: "absolute",
      left: "-.5em",
      top: "-.5em",
      borderImage:
        "url('https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/OG42-simpleBorder.svg') repeat",
      borderImageSlice: "50% fill",
      borderImageWidth: ".5em ",
      content: "' '",
      width: "calc(100% + 1em)",
      transform: "scale(.9)",
      height: "calc(100% + 1em)",
      opacity: 0,
      transition: ".3s all"
    },

    "&:hover:before": {
      transform: "scale(1)",
      opacity: 1,
      transition: ".3s all"
    }
  },
  media: {
    height: "100%",
    width: "100%",
    overflow: "hidden",
    position: "absolute",
    opacity: 0.3,
    borderRadius: "0 !important"
  }
});

function FlatButtons(props) {
  const { classes } = props;
  return (
    <div className={classes.main}>
      {" "}
      <Typography className={classes.title} variant="headline">
        EVENTS
      </Typography>
      <div>
        <Card className={classes.card}>
          <CardHeader
            action={
              <IconButton>
                <MoreVertIcon />
              </IconButton>
            }
            title="Shrimp and Chorizo Paella"
            subheader="September 14, 2016"
          />
          <CardMedia
            className={classes.media}
            image="https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/4Fw7-powerplant-50.jpg"
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="title">
              Event Title
            </Typography>
            <Typography component="p">
              Lizards are a widespread group of squamate reptiles, with over
              6,000 species, ranging across all continents except Antarctica
            </Typography>
          </CardContent>
          <CardActions>
            <Button color="primary">Share</Button>
            <Button color="primary">Learn More</Button>
          </CardActions>
        </Card>
        <Card className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/4Fw7-powerplant-50.jpg"
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="title">
              Event Title
            </Typography>
            <Typography component="p">
              Lizards are a widespread group of squamate reptiles, with over
              6,000 species, ranging across all continents except Antarctica
            </Typography>
          </CardContent>
          <CardActions>
            <Button color="primary">Share</Button>
            <Button color="primary">Learn More</Button>
          </CardActions>
        </Card>
      </div>{" "}
      <NewEvent className={classes.newevent} />
    </div>
  );
}
FlatButtons.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FlatButtons);
