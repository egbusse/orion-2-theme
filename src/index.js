import React from "react";
import { render } from "react-dom";
import Flatbuttons from "./components/flatbuttons";
import Raisedbuttons from "./components/raisedbuttons";
import Outlinebuttons from "./components/outlinebuttons";
import Stepper from "./components/stepper";
import Textfields from "./components/textfields";
import Selects from "./components/selects";
import MultipleSelect from "./components/multipleSelect";
import Tables from "./components/tables";
import theme from "./theme/style.js";
import Menu from "./components/menu.js";
import Mock from "./components/mock.js";
import Subscriptions from "./subscriptions/subscriptions.js";

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

const rootElement = document.querySelector("#root");
if (rootElement) {
  render(
    <MuiThemeProvider theme={theme}>
      <Flatbuttons />
      <Outlinebuttons />
      <Raisedbuttons />

      <Textfields />
      <Selects />
      <MultipleSelect />
      <Menu />
    </MuiThemeProvider>,

    rootElement
  );
}
