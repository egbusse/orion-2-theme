import { createMuiTheme } from "@material-ui/core/styles";

import { colorDarkest } from "./colors.js";
import { colorBlack } from "./colors.js";
import { colorDarkBlue } from "./colors.js";
import { colorMainAccent } from "./colors.js";
import { colorGold } from "./colors.js";
import { colorOrange } from "./colors.js";
import { colorRed } from "./colors.js";
import { colorLightest } from "./colors.js";
import { colorGrey } from "./colors.js";
import { raisedbutton } from "./backgrounds.js";
import { pressedbutton } from "./backgrounds.js";
import { inputBG } from "./backgrounds.js";
import { navBG } from "./backgrounds.js";

const theme = createMuiTheme({
  typography: {
    // Use the system font.
    fontFamily: '"Barlow Condensed",Arial,sans-serif',
    subheading: {}
  },
  palette: {
    primary: {
      main: colorOrange,
      dark: colorOrange,
      contrastText: colorLightest
    },
    secondary: {
      main: colorDarkBlue
    },
    background: {
      paper: "#00000000",
      default: "#00000000"
    },
    type: "dark"
  },
  overrides: {
    MuiCardHeader: {
      title: {
        outline: "none",
        border: "none",
        background: "none"
      }
    },

    MuiInputLabel: {
      formControl: { transform: "translate(.4em, 24px) scale(1)" }
    },

    MuiDialog: {
      paper: {
        border: "solid 1px " + colorLightest,
        borderRadius: "0",
        background: colorGrey
      }
    },
    MuiInput: {
      input: {
        padding: ".4em",
        borderImage:
          "url('data:image/svg+xml;utf8," +
          inputBG(colorLightest, colorLightest, colorRed) +
          "')",
        borderImageSlice: "33% fill",
        borderImageWidth: "4px"
      },

      disabled: {
        background: "rgba(256,256,256,.5)"
      },
      underline: {
        "&:before": {
          //underline color when textfield is inactive
          borderImage:
            "url('data:image/svg+xml;utf8," +
            inputBG(colorLightest, colorLightest, colorLightest) +
            "')",
          borderImageSlice: "33% fill",
          borderImageWidth: "4px",
          top: "0"
        },

        "&:after": {
          borderImage:
            "url('data:image/svg+xml;utf8," +
            inputBG(colorOrange, colorLightest, colorOrange) +
            "')",
          borderImageSlice: "33% fill",
          borderImageWidth: "4px",
          top: "0",
          transformOrigin: "50% 50%"
        },
        "&:hover:not($disabled):before": {
          //underline color when hovered
          borderImage:
            "url('data:image/svg+xml;utf8," +
            inputBG(colorOrange, colorLightest, colorLightest) +
            "')",
          borderImageSlice: "33% fill",
          borderImageWidth: "4px"
        },
        "&$error:after": {
          borderImage:
            "url('data:image/svg+xml;utf8," +
            inputBG(colorRed, colorLightest, colorRed) +
            "')",
          borderImageSlice: "33% fill",
          borderImageWidth: "4px",
          top: "0"
        }
      }
    },
    MuiSelect: {
      select: {
        borderImage: "none !important",
        borderStyle: "solid",
        padding: ".5em 1.5em .5em .9em !important",
        background: "none",
        borderWidth: "1px 0px 1px 0px !important"
      }
    },

    MuiList: {
      root: { background: colorDarkest }
    },
    MuiTypography: {
      body1: { fontWeight: "300" },
      title: {
        fontWeight: "400",

        color: colorLightest,
        textTransform: "uppercase",

        padding: ".2em",
        letterSpacing: ".3em"
      },

      subheading: {
        letterSpacing: ".2em"
      },

      headline: {
        fontWeight: "400",

        color: colorLightest,
        background: colorLightest + "44",
        textTransform: "uppercase",
        fontSize: "1.3em",
        padding: ".2em",
        minWidth: "200px",
        letterSpacing: ".3em",
        border: "5px solid " + colorDarkest,
        outline: "1px solid " + colorLightest
      }
    },
    MuiButton: {
      root: {
        borderRadius: 0,

        letterSpacing: ".3em",

        padding: "0 .5em"
      },
      label: {
        margin: "0",
        minHeight: "36px",
        "&:active": {
          transform: "Scale(.95)"
        },
        "&:focus": {
          transform: "Scale(.95)"
        }
      },

      flatPrimary: {},

      outlined: {
        border: "solid 1px",

        borderRadius: 0
      },
      raised: {
        color: colorLightest,
        background: "none",
        borderRadius: 0,
        borderImage:
          "url('data:image/svg+xml;utf8," +
          raisedbutton(colorOrange, colorLightest, colorLightest) +
          "')",
        borderImageSlice: "33%  fill",
        borderImageWidth: "8px 8px",
        boxShadow: "none",
        padding: "0 .9em",
        "&:hover": {
          boxShadow: "inset 0 0 0px .6em " + colorDarkest
        },
        "&:focus": {
          boxShadow: "inset 0 0 0px .6em " + colorDarkest
        },
        "&:active": {
          boxShadow: "inset 0 0 0px .6em " + colorDarkest
        },
        "&$disabled": {
          borderImage:
            "url('data:image/svg+xml;utf8," +
            raisedbutton(colorLightest, colorLightest, colorLightest) +
            "')",
          borderImageSlice: "33%  fill",
          borderImageWidth: "8px 8px",
          boxShadow: "none"
        }
      },
      raisedPrimary: {
        borderImage:
          "url('data:image/svg+xml;utf8," +
          raisedbutton(colorOrange, colorLightest, colorLightest) +
          "')",
        borderImageSlice: "33%  fill",
        borderImageWidth: "8px 8px",

        "&:hover": {
          borderImage:
            "url('data:image/svg+xml;utf8," +
            raisedbutton(colorOrange, colorLightest, colorOrange) +
            "')",
          borderImageSlice: "33%  fill",
          borderImageWidth: "8px 8px"
        }
      },
      raisedSecondary: {
        borderImage:
          "url('data:image/svg+xml;utf8," +
          raisedbutton(colorRed, colorLightest, colorLightest) +
          "')",
        borderImageSlice: "33%  fill",
        borderImageWidth: "8px 8px",

        "&:hover": {
          borderImage:
            "url('data:image/svg+xml;utf8," +
            raisedbutton(colorRed, colorLightest, colorRed) +
            "')",
          borderImageSlice: "33%  fill",
          borderImageWidth: "8px 8px"
        }
      }
    }
  }
});
export default theme;
