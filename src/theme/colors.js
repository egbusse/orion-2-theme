//COLOR VARIABLES
const colorBlack = "#000000";
const colorDarkBlue = "#ff5555";
const colorDarkest = "#111111";
const colorMainAccent = "#005500";
const colorGold = "#ff9e1b";
const colorOrange = "#fc9a1b";
const colorRed = "#ff514d";
const colorLightest = "#f2f2f2";
const colorDark2 = "#0e3c54";
const colorGrey = "rgb(24,24,24)";

export { colorBlack };
export { colorDarkBlue };
export { colorDarkest };
export { colorMainAccent };
export { colorGold };
export { colorOrange };
export { colorRed };
export { colorLightest };
export { colorDark2 };
export { colorGrey };
