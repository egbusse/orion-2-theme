import { colorLightBlue } from "./colors.js";
import { colorDarkest } from "./colors.js";
import { colorOrange } from "./colors.js";
import { colorMainAccent } from "./colors.js";
import { colorGrey } from "./colors.js";
import { colorGold } from "./colors.js";

import { colorLightest } from "./colors.js";

//BACKGROUND IMAGES
export function raisedbutton(e, d, f) {
  var raisedbutton =
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 9"><path d="M8.5 8.5H1v-8h5.74L8.5 2.22z" stroke-width=".5" stroke="' +
    d +
    '" fill="none" stroke-miterlimit="10"/><path stroke="' +
    e +
    '" stroke-width="1" fill="none" stroke-miterlimit="10" d="M1 5.75v-2.5"/><path fill="' +
    f +
    '" d="M9 1.83V0H7.19L9 1.83z"/></svg>';
  return raisedbutton;
}

export function inputBG(e, d, f) {
  var inputBG =
    '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 9"><g data-name="Layer 1" fill="none"><path d="M8 8.5H1v-8h7z" stroke="' +
    d +
    '" stroke-width=".5"/><path stroke-width="1.5" stroke="' +
    e +
    '" d="M1 5.75v-2.5"/><path stroke="' +
    f +
    '" stroke-width="1.5" d="M8 3.25v2.5"/></g></svg>';
  return inputBG;
}
var hexBG =
  '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6 6"><path fill="' +
  colorGrey +
  '" stroke="#fc9a1b" stroke-miterlimit="10" stroke-width=".25"  d="M5.47 4.42V1.57L3 .15.53 1.57v2.85L3 5.85l2.47-1.43z"/></svg>';
var navBG =
  '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 9" stroke-width=".5"><defs><style>.cls-1{fill:"none";stroke:' +
  colorLightest +
  ';stroke-miterlimit:10}</style></defs><path class="cls-1" d="M6.5.5h2v2M8.5 3.5v2M8.5 6.5v2h-2M5.5 8.5h-2M2.5 8.5h-2v-2M.5 2.5v-2h2M3.5.5h2M.5 5.5v-2"/></svg>';

//export { inputBG };
export { navBG };
export { hexBG };
