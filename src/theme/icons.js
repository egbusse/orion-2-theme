import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';








export function UndoIcon(props) {
  return (
    <SvgIcon>
      <path class="cls-1" d="M15.91 15h-1v-2a3.5 3.5 0 0 0-3.5-3.5H.8v-1h10.61a4.5 4.5 0 0 1 4.5 4.5z" />
      <path class="cls-1" d="M4.44 13.35L.09 9l4.35-4.35.71.7L1.5 9l3.65 3.65-.71.7zM23.91 19.5h-15v-7h1v6h13v-13h-13v1h-1v-2h15v15z" />

    </SvgIcon>
  );
}


export function RedoIcon(props) {
  return (
    <SvgIcon>
      <path class="cls-1" d="M9.09 15h-1v-2a4.5 4.5 0 0 1 4.5-4.5H23.2v1H12.59a3.5 3.5 0 0 0-3.5 3.5z" />
      <path class="cls-1" d="M19.56 13.35l-.71-.7L22.5 9l-3.65-3.65.71-.7L23.91 9l-4.35 4.35zM15.09 19.5h-15v-15h15v2h-1v-1h-13v13h13v-6h1v7z" />

    </SvgIcon>
  );
}



export function SaveIcon(props) {
  return (
    <SvgIcon>
      <path class="cls-1" d="M20.69 20.79H3.1V3.21h14.2l3.38 3.38zm-16.59-1h15.59V7l-2.8-2.8H4.1z" />
      <path class="cls-1" d="M17 7.86H6.28V3.21H17zm-9.7-1H16V4.21H7.28zM11.89 14.75A2.75 2.75 0 1 1 14.64 12a2.75 2.75 0 0 1-2.75 2.75zm0-4.5A1.75 1.75 0 1 0 13.64 12a1.75 1.75 0 0 0-1.75-1.75z" />

    </SvgIcon>
  );
}


export function MinimizeIcon(props) {
  return (
    <SvgIcon>
      <path class="cls-1" d="M17.33 13.15h-5.79V7.36h1v4.79h4.79v1z" />
      <path class="cls-1" d="M20.84 20.79H3.25V3.21h17.59zm-16.59-1h15.59V4.21H4.25z" />
      <path class="cls-1" transform="rotate(-45 16.192 8.503)" d="M10.32 8h11.73v1H10.32z" />
    </SvgIcon>
  );
}



export function ProfileIcon(props) {
  return (
    <SvgIcon>
      <path class="cls-1" d="M12 14.79a4 4 0 1 1 4-4 4 4 0 0 1-4 4zm0-7a3 3 0 1 0 3 3 3 3 0 0 0-3-3zM12 24a11.88 11.88 0 0 1-5.41-1.3.5.5 0 0 1-.26-.55C7.17 18 9.45 15.29 12 15.29s4.83 2.76 5.68 6.86a.5.5 0 0 1-.26.55A11.86 11.86 0 0 1 12 24zm-4.61-2a10.93 10.93 0 0 0 9.23 0c-.79-3.39-2.62-5.71-4.62-5.71S8.17 18.61 7.39 22z" />
      <path class="cls-1" d="M12 24a12 12 0 1 1 12-12 12 12 0 0 1-12 12zm0-23a11 11 0 1 0 11 11A11 11 0 0 0 12 1z" />
    </SvgIcon>
  );
}

