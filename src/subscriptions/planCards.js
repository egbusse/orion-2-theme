import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { colorOrange } from "../theme/colors.js";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  main: {  position:"relative",  width:"400px"},
  card: {
    display:"inline-block",
    width: "20%",
    minWidth:200,
    position: "relative",
    borderRadius:"1em",
    left:"50%",
    transform:"translateX(-50%)",
    pointerEvents:"none",
   zIndex: "2",
   transition: ".3s all",
   cursor:"pointer"
  
  },
  media: {
    width: "100%",
    height: 0,
    paddingTop: "56.25%", // 16:9
    position: "absolute"
  },

  plan: {
    pointerEvents: "none",
    position: "relative",
    height: "100%",
    fontSize: "1.5em",
    color: "#f2f2f2",
    textShadow:"0 0 10px rgba(0,0,0,1)",
    width: "10ch",
    zIndex: "10", 
    lineHeight:"1em",
    
  },
  center: { textAlign: "center",  paddingTop: "26.25%",  },
  price: { color: colorOrange, display: "inline-block", fontSize: "2.5em", pointerEvents: "none", },
  month: {
    pointerEvents: "none",
    color: colorOrange,
    display: "inline-block",
    width: "5ch",
    lineHeight: "1em;",
    fontSize: "1em",
    textAlign: "left"
  },
  disabled:{
pointerEvents:"initial",
position:"absolute",
opacity:.5,
zIndex: "0",
transition: ".3s all",

transform: "translateX(-80%)",
 "&:hover": {
   transform: "translateX(-100%) rotate(2deg)",

 
  transition:".3s all"
},
   

  }
});

class PlanCards extends React.Component {


  state = {
    unlimited: true,
  };


  handleClick = () => {
    this.setState(prevState => ({
      unlimited: !prevState.unlimited
     
    })); 
  };

  render() {

   
    const { classes } = this.props;

    return (
      <div className={classes.main} onClick={this.handleClick} >
        <Card className={[classes.card,  this.state.unlimited === true ? classes.disabled : " "]}>
          <CardMedia
            className={classes.media}
            image="https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/OXc--unlimited.jpg"
            title="Ares Splash"
          />
          <CardContent>
            <Typography variant="subheading" className={classes.plan}>
              Ares Unlimited Monthly
            </Typography>
            <div className={classes.center}>
              <Typography variant="subheading" className={classes.price}>
                $399
              </Typography>
              <Typography variant="subheading" className={classes.month}>
                PER MONTH
              </Typography>
            </div>
          </CardContent>
        </Card>
        <Card className={[classes.card, this.state.unlimited === true ? " " : classes.disabled]}>
          <CardMedia
            className={classes.media}
            image="https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/mZV0-motnhly.jpg"
            title="Ares Splash"
          />
          <CardContent>
            <Typography variant="subheading" className={classes.plan}>
              Ares Apprentice Plan
            </Typography>
            <div className={classes.center}>
              <Typography variant="subheading" className={classes.price}>
                $39
              </Typography>
              <Typography variant="subheading" className={classes.month}>
                PER MONTH
              </Typography>
            </div>
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(PlanCards);
