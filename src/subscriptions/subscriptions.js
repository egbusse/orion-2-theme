import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import FormHelperText from "@material-ui/core/FormHelperText";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import PlanCards from "./planCards.js";

const styles = theme => ({
  
  main: {
    background:
      "url(https://uploads.codesandbox.io/uploads/user/02ec36a2-0417-43b0-afc5-148536f9ee32/QRMZ-subsBG.jpg)no-repeat right bottom fixed",
    backgroundSize: "cover",
    height: "100vh",
    width: "100%",
    overflowY:"hidden",
    position: "relative",
    display:"flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  paper: {
    position: "relative",
   
    height: "70vh",
    minHeight: "700px",
    maxHeight:"800px",
    
    width: "56vw",
    maxWidth: "800px",
    zIndex: "0",
    background: "#f6f9fc",
    display: "flex",
    flexDirection: "column",
    //THE DIAGONAL

    "&:before": {
      position: "absolute",
     
      transform: "skewX(27deg) translateX(-35%)",
      content: "' '",
      width: "200%",
      height: "100%",
      background: "#f6f9fc",
      zIndex: "-1",
      boxShadow: "inherit"
    }
  },
  cardwrapper:{
    margin: "1em auto 2em",
    display:"flex",
    justifyContent: "center",
    width:"400px",
    height:"200px",
    border:"solid 1px #8d847a",
    borderRadius:"1em",
    alignItems: "center",
    padding:"0 0 2em",

  
  },
  textField: {
    width: "100%;"
  },
  buttonwrap:{
  
    width:"400px",
    display:"flex",
   
    justifyContent: "space-between",
    marginBottom:"1em"
  },
  button:{width:"150px"},
  wrapper: {
    
    display:"flex",
    alignItems: "center",
  
    flexGrow: "1",
    flexDirection: "column"
  },
  title:{
    margin: "2em",
  },

});

class Subscriptions extends React.Component {


  render() {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <Paper className={classes.paper} elevation={4}>
          <Typography variant="title" gutterBottom className={classes.title}>
            SUBSCRIPTION PAYMENT
            </Typography>
          <div className={classes.wrapper}>
         
         <PlanCards/>
         
            <div className={classes.cardwrapper}>
              <form className={classes.container} noValidate autoComplete="off">
                <TextField
                  id="number"
                  label="CARD NUMBER"
                  className={classes.textField}
                  type="number"
                 
                  margin="normal"
                /><br/>
                <TextField
                  id="date"
               
               
                  type="date"
                 
                  margin="normal"
                />  <TextField
                  id="cvc"
                  label="CVC"
                 
                  type="number"
                
                  margin="normal"
                /><br /><TextField
                  id="zip"
                  label="POSTAL CODE"
                  className={classes.textField}
                  type="number"
                
                  margin="normal"
                />
            
              </form>
              
            </div>
            <div className={classes.buttonwrap}>
              <Button variant="outlined" className={classes.button} color="primary">
             CANCEL
          </Button>
            <Button variant="outlined" className={classes.button} color="primary">
              PAY
          </Button></div>
          </div>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Subscriptions);
